class Product < ApplicationRecord
	has_many :order_products

	default_scope { where(active: true) }
  def self.search(search)
	where("name LIKE ?", "%#{search}%")
  end
end
