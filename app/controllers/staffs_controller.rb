class StaffsController < ApplicationController
  def index
    @staff = Staff.all
  end
  def update
    @staff  = Staff.find(params[:id])
    if @staff.update(staff_params)
      redirect_to @staff
    else
      render 'edit'
    end
  end
  def show
    @staff  = Staff.find(params[:id])
  end

  def destroy
      @staff  = Staff.find(params[:id])
      @staff.destroy
      redirect_to managers_listing_path
  end

  def edit
        @staff  = Staff.find(params[:id])
  end
  def new
        @staff  = Staff.new
  end
  def create
    @staff  = Staff.new
    if @staff.save
      redirect_to @staff
    else
      render 'new'
    end
  end
private
  def staff_params
    params.require(:staff).permit(:name, :email, :manager)
  end
end
