class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  helper_method :current_order
  before_filter :configure_permitted_parameters, if: :devise_controller?

  
  def current_order
	if !session[:order_id].nil?
	  Order.find(session[:order_id])
	else
	  Order.new
	end
  end
  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:shipping_address, :billing_address, :name, :username ])
  end
end
