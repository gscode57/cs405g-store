class ProductsController < ApplicationController
  
  def index
    @products = Product.all
    @order_product = current_order.order_products.new
    if params[:search]
	@products = Product.search(params[:search]).order("name")
    else
	@products = Product.all.order('name')
    end
  end
  def update
    @product = Product.find(params[:id])
    if @product.update(product_params)
      redirect_to @product
    else
      render 'edit'
    end
  end 
  def show
    @product=Product.find(params[:id])
  end

  def destroy
      @product = Product.find(params[:id])
      @product.destroy
      redirect_to products_listing_path
  end

  helper_method :productshowall
  def productshowall
	result = ActiveRecord::Base.connection.execute("SELECT * FROM products")
	return result
  end
  def edit
	@product = Product.find(params[:id])
  end
  def new
	@product = Product.new
  end
  def create
    @product = Product.new(product_params)
    if @product.save
      redirect_to @product
    else
      render :new
    end 
  end
private
  def product_params
    params.require(:product).permit(:name, :price, :seller, :category, :stock, :discount)
  end
end
