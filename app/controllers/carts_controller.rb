class CartsController < ApplicationController
  def show
    @order_products = current_order.order_products
  end
  def checkout
    @order_products = current_order.order_products
    s_total = 0
    name = "" 
    amount = 0
    product_desc = ""
    @order_products.each_with_index do |product_iter, index| 
        product = Product.find(product_iter.product_id)
	amount = product_iter.quantity
	if index == @order_products.size - 1 then
	    product_desc = product.name + " " + amount.to_s + ", " + product_desc
	else 
	    product_desc = product_desc + product.name + " " + amount.to_s 
	end
    end
    current_order.update_attributes(  :user_id => current_user.id, :tax => current_order.subtotal * 0.06, :total => current_order.subtotal * 1.06, :description => product_desc, :time => Time.now)
    @order_products.each do |buy_product|
	product = Product.find(buy_product.product_id)
	product.update_attribute(:stock, (product.stock - buy_product.quantity))
	s_total = s_total + (buy_product.quantity * buy_product.unit_price)
	buy_product.delete
    end
    
    session[:order_id] = nil #this forces a new order to be created
    
  end
	
end
