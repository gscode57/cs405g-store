Rails.application.routes.draw do
  get 'statistics/listing'
  get 'statistics/weekly'
  get 'statistics/monthly'
  get 'statistics/yearly'

  get 'staff_members/show'
  get 'staff_members/listing'
  get 'staff_members/edit'
  get 'staff_members/index'
  get 'staff_members/destroy'
  get 'staffs/destroy'
  get 'staffs/show'

  get 'carts/checkout'
  post 'carts/checkout'
  get 'orders/show'
  
  get 'orders/destroy'

  get 'orders/create'
  get 'orders/ship'
  post 'orders/ship'
  devise_for :staffs
  
  get 'order_products/create'

  get 'order_products/update'

  get 'order_products/destroy'
  get 'order_products/new_thing'

  get 'carts/show'
  get 'products/show', to: 'products#listing'
  get 'products/index'
  get 'products/edit'
  get 'products/listing'
  get 'products/destroy'
  get 'products/destroy'
  #post 'products/destroy'
  get 'products/new'
  post 'products/new'
  devise_for :users
  resources :staffs
  resources :staff_members
  resources :products#, only: [:new,:index, :edit, :listing]
  resources :carts, only: [:show, :checkout]
  resources :order_products, only: [:create,:new_thing,  :update, :destroy], defaults: {format: 'js'}
  resources :orders
  root 'products#index'
  #root 'home#index' ==> main home page
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
