class FixUnitPRice < ActiveRecord::Migration[5.0]
  def change
	rename_column :order_products, :subtotal, :unit_price
  end
  
  
end
