class AddUserNameToStaff < ActiveRecord::Migration[5.0]
  def change
    add_column :staffs, :user_name, :string
  end
end
