class CreateProducts < ActiveRecord::Migration[5.0]
  def change
    create_table :products do |t|
      t.string :name
      t.decimal :price, precision: 12, scale: 3
      t.string :seller
      t.string :category
      t.string :seller
      t.string :picture_file
      t.integer :stock

      t.timestamps
    end
  end
end
