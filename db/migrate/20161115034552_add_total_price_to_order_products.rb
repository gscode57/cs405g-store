class AddTotalPriceToOrderProducts < ActiveRecord::Migration[5.0]
  def change
    add_column :order_products, :total_price, :decimal, precision: 12, scale: 3
  end
end
