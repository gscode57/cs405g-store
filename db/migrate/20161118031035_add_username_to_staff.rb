class AddUsernameToStaff < ActiveRecord::Migration[5.0]
  def change
    add_column :staffs, :username, :string
  end
end
